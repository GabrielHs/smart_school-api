using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using smartschool_api.Data;
using smartschool_api.Models;
using smartschool_api.Services;

namespace smartschool_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AlunoController : ControllerBase
    {
        private readonly IRepository _respo;
        public AlunoController(IRepository respo)
        {
            _respo = respo;

        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] OwnerParametersService ownerParameters)
        {
            try
            {
                var result = await _respo.GetAllAlunosAsync(true, ownerParameters);

                var metadata = new
                {
                    result.TotalCount,
                    result.PageSize,
                    result.CurrentPage,
                    result.TotalPages,
                    result.HasNext,
                    result.HasPrevious,
                };
                return Ok(new { metadata, data = result });

            }
            catch (Exception ex)
            {

                return BadRequest($"Erro {ex.Message}");
            }
        }

        [HttpGet("ByDisciplina/{disciplinaId}")]
        public async Task<IActionResult> GetByDisciplinaId(int disciplinaId)
        {
            try
            {
                var result = await _respo.GetAlunosAsyncByDisciplinaId(disciplinaId, false);
                return Ok(result);

            }
            catch (Exception ex)
            {

                return BadRequest($"Erro {ex.Message}");
            }
        }


        [HttpPost]
        public async Task<IActionResult> Post(AlunoModel model)
        {
            try
            {
                _respo.Add(model);

                if (await _respo.SaveChangesAsync())
                {
                    // return CreatedAtAction(nameof(Post), new { ok = "criado", aluno = model });
                    return CreatedAtAction(nameof(Post), new { model });
                }


            }
            catch (Exception ex)
            {

                return BadRequest($"Erro {ex.Message}");
            }

            return BadRequest(new { erro = "não criado usuario" });

        }


        [HttpPut("{AlunoId}")]
        public async Task<IActionResult> Put(int AlunoId, AlunoModel model)
        {
            try
            {
                var aluno = await _respo.GetAlunoAsyncById(AlunoId, false);

                if (aluno == null) return NotFound(new { info = "aluno não encontrado" });

                _respo.Update(model);


                if (await _respo.SaveChangesAsync())
                {
                    return Ok(new { ok = "atualizado", aluno = model });

                }


            }
            catch (Exception ex)
            {

                return BadRequest($"Erro {ex.Message}");
            }

            return BadRequest(new { erro = "não criado usuario" });

        }


        [HttpDelete("{AlunoId}")]
        public async Task<IActionResult> Delete(int AlunoId)
        {
            try
            {
                var aluno = await _respo.GetAlunoAsyncById(AlunoId, false);

                if (aluno == null) return NotFound(new { info = "aluno não encontrado" });

                _respo.Delete(aluno);


                if (await _respo.SaveChangesAsync())
                {
                    return Ok(new { ok = "deletado" });

                }


            }
            catch (Exception ex)
            {

                return BadRequest($"Erro {ex.Message}");
            }

            return BadRequest(new { erro = "não criado usuario" });

        }
    }
}