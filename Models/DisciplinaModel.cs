using System.Collections.Generic;

namespace smartschool_api.Models
{
    public class DisciplinaModel
    {
        public DisciplinaModel() { }
        public DisciplinaModel(int id, string nome, int professorId)
        {
            this.Id = id;
            this.Nome = nome;
            this.ProfessorId = professorId;

        }
        public int Id { get; set; }

        public string Nome { get; set; }

        public int ProfessorId { get; set; }

        public ProfessorModel Professor { get; set; }

        public IEnumerable<AlunoDisciplinaModel> AlunoDisciplinaModels { get; set; }

    }
}