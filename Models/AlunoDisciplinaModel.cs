namespace smartschool_api.Models
{
    public class AlunoDisciplinaModel
    {
        public AlunoDisciplinaModel() { }
        public AlunoDisciplinaModel(int alunoId, int disciplinaId)
        {
            this.AlunoId = alunoId;
            this.DisciplinaId = disciplinaId;

        }
        public int AlunoId { get; set; }
        public AlunoModel Aluno { get; set; }

        public int DisciplinaId { get; set; }
        public DisciplinaModel Disciplina { get; set; }
    }
}