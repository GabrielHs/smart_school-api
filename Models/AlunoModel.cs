using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace smartschool_api.Models
{
    public class AlunoModel
    {
        public AlunoModel(){ }
        public AlunoModel(int id, string nome, string sobreNome, string telefone)
        {
            this.Id = id;
            this.Nome = nome;
            this.SobreNome = sobreNome;
            this.Telefone = telefone;

        }
        public int Id { get; set; }

        public string Nome { get; set; }


        public string SobreNome { get; set; }


        public string Telefone { get; set; }

        public IEnumerable<AlunoDisciplinaModel> AlunoDisciplinaModels { get; set; }

    }
}