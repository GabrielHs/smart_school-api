
using System.Collections.Generic;

namespace smartschool_api.Models
{

    public class ProfessorModel
    {
        public ProfessorModel() { }
        public ProfessorModel(int id, string nome)
        {
            this.Id = id;
            this.Nome = nome;

        }
        public int Id { get; set; }

        public string Nome { get; set; }

        public IEnumerable<DisciplinaModel> Disciplinas { get; set; }

    }
}