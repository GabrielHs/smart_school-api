using System.Threading.Tasks;
using smartschool_api.Models;
using smartschool_api.Services;

namespace smartschool_api.Data
{
    public interface IRepository
    {
        //GERAL
        void Add<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveChangesAsync();

        //ALUNO
        Task<PagedList<AlunoModel>> GetAllAlunosAsync(bool includeProfessor, OwnerParametersService ownerParameters = null);
        Task<AlunoModel[]> GetAlunosAsyncByDisciplinaId(int disciplinaId, bool includeDisciplina);
        Task<AlunoModel> GetAlunoAsyncById(int alunoId, bool includeProfessor);

        //PROFESSOR
        Task<PagedList<ProfessorModel>> GetAllProfessoresAsync(bool includeAluno, OwnerParametersService ownerParameters = null);
        Task<ProfessorModel> GetProfessorAsyncById(int professorId, bool includeAluno);
        Task<ProfessorModel[]> GetProfessoresAsyncByAlunoId(int alunoId, bool includeDisciplina);
    }
}