using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using smartschool_api.Models;
using smartschool_api.Services;

namespace smartschool_api.Data
{
    public class Repository : IRepository
    {
        private readonly DataContext _context;

        public Repository(DataContext context)
        {
            _context = context;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }
        public void Update<T>(T entity) where T : class
        {
            _context.Update(entity);
        }
        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }
        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }

        public async Task<PagedList<AlunoModel>> GetAllAlunosAsync(bool includeDisciplina = false, OwnerParametersService ownerParameters = null)
        {
            IQueryable<AlunoModel> query = _context.Alunos;

            if (includeDisciplina)
            {
                query = query.Include(pe => pe.AlunoDisciplinaModels)
                             .ThenInclude(ad => ad.Disciplina)
                             .ThenInclude(d => d.Professor);
            }

            if (ownerParameters == null)
            {
                query = query.AsNoTracking().OrderBy(c => c.Id);

                return await PagedList<AlunoModel>.ToPagedListAsync(query.AsNoTracking().OrderBy(c => c.Id), 0, 0);
            }

            if (!String.IsNullOrEmpty(ownerParameters.Search))
            {
                query = query.Where(s => s.Nome.Contains(ownerParameters.Search)
                                       || s.SobreNome.Contains(ownerParameters.Search));
            }
            return await PagedList<AlunoModel>.ToPagedListAsync(query.AsNoTracking().OrderBy(c => c.Id), ownerParameters.PageNumber, ownerParameters.PageSize);

        }
        public async Task<AlunoModel> GetAlunoAsyncById(int alunoId, bool includeDisciplina)
        {
            IQueryable<AlunoModel> query = _context.Alunos;

            if (includeDisciplina)
            {
                query = query.Include(a => a.AlunoDisciplinaModels)
                             .ThenInclude(ad => ad.Disciplina)
                             .ThenInclude(d => d.Professor);
            }

            query = query.AsNoTracking()
                         .OrderBy(aluno => aluno.Id)
                         .Where(aluno => aluno.Id == alunoId);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<AlunoModel[]> GetAlunosAsyncByDisciplinaId(int disciplinaId, bool includeDisciplina)
        {
            IQueryable<AlunoModel> query = _context.Alunos;

            if (includeDisciplina)
            {
                query = query.Include(p => p.AlunoDisciplinaModels)
                             .ThenInclude(ad => ad.Disciplina)
                             .ThenInclude(d => d.Professor);
            }

            query = query.AsNoTracking()
                         .OrderBy(aluno => aluno.Id)
                         .Where(aluno => aluno.AlunoDisciplinaModels.Any(ad => ad.DisciplinaId == disciplinaId));

            return await query.ToArrayAsync();
        }

        public async Task<ProfessorModel[]> GetProfessoresAsyncByAlunoId(int alunoId, bool includeDisciplina)
        {
            IQueryable<ProfessorModel> query = _context.Professores;

            if (includeDisciplina)
            {
                query = query.Include(p => p.Disciplinas);
            }

            query = query.AsNoTracking()
                         .OrderBy(aluno => aluno.Id)
                         .Where(aluno => aluno.Disciplinas.Any(d =>
                            d.AlunoDisciplinaModels.Any(ad => ad.AlunoId == alunoId)));

            return await query.ToArrayAsync();
        }

        public async Task<PagedList<ProfessorModel>> GetAllProfessoresAsync(bool includeDisciplinas = true, OwnerParametersService ownerParameters = null)
        {
            IQueryable<ProfessorModel> query = _context.Professores;

            if (includeDisciplinas)
            {
                query = query.Include(c => c.Disciplinas);
            }

            if (ownerParameters == null)
            {
                query = query.AsNoTracking().OrderBy(c => c.Id);

                return await PagedList<ProfessorModel>.ToPagedListAsync(query.AsNoTracking().OrderBy(c => c.Id), 0, 0); ;
            }


           if (!String.IsNullOrEmpty(ownerParameters.Search))
            {
                query = query.Where(s => s.Nome.Contains(ownerParameters.Search));
            }

            return await PagedList<ProfessorModel>.ToPagedListAsync(query.AsNoTracking().OrderBy(c => c.Id), ownerParameters.PageNumber, ownerParameters.PageSize);

        }
        public async Task<ProfessorModel> GetProfessorAsyncById(int professorId, bool includeDisciplinas = true)
        {
            IQueryable<ProfessorModel> query = _context.Professores;

            if (includeDisciplinas)
            {
                query = query.Include(pe => pe.Disciplinas);
            }

            query = query.AsNoTracking()
                         .OrderBy(professor => professor.Id)
                         .Where(professor => professor.Id == professorId);

            return await query.FirstOrDefaultAsync();
        }
    }
}
