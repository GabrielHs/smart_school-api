using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using smartschool_api.Models;

namespace smartschool_api
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<AlunoModel> Alunos { get; set; }
        public DbSet<ProfessorModel> Professores { get; set; }
        public DbSet<DisciplinaModel> Disciplinas { get; set; }
        public DbSet<AlunoDisciplinaModel> AlunosDisciplinas { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<AlunoDisciplinaModel>()
                .HasKey(AD => new { AD.AlunoId, AD.DisciplinaId });

            builder.Entity<ProfessorModel>()
                .HasData(new List<ProfessorModel>(){
                    new ProfessorModel(1, "Lauro"),
                    new ProfessorModel(2, "Roberto"),
                    new ProfessorModel(3, "Ronaldo"),
                    new ProfessorModel(4, "Rodrigo"),
                    new ProfessorModel(5, "Alexandre"),
                });

            builder.Entity<DisciplinaModel>()
                .HasData(new List<DisciplinaModel>{
                    new DisciplinaModel(1, "Matemática", 1),
                    new DisciplinaModel(2, "Física", 2),
                    new DisciplinaModel(3, "Português", 3),
                    new DisciplinaModel(4, "Inglês", 4),
                    new DisciplinaModel(5, "Programação", 5)
                });

            builder.Entity<AlunoModel>()
                .HasData(new List<AlunoModel>(){
                    new AlunoModel(1, "Marta", "Kent", "33225555"),
                    new AlunoModel(2, "Paula", "Isabela", "3354288"),
                    new AlunoModel(3, "Laura", "Antonia", "55668899"),
                    new AlunoModel(4, "Luiza", "Maria", "6565659"),
                    new AlunoModel(5, "Lucas", "Machado", "565685415"),
                    new AlunoModel(6, "Pedro", "Alvares", "456454545"),
                    new AlunoModel(7, "Paulo", "José", "9874512")
                });

            builder.Entity<AlunoDisciplinaModel>()
                .HasData(new List<AlunoDisciplinaModel>() {
                    new AlunoDisciplinaModel() {AlunoId = 1, DisciplinaId = 2 },
                    new AlunoDisciplinaModel() {AlunoId = 1, DisciplinaId = 4 },
                    new AlunoDisciplinaModel() {AlunoId = 1, DisciplinaId = 5 },
                    new AlunoDisciplinaModel() {AlunoId = 2, DisciplinaId = 1 },
                    new AlunoDisciplinaModel() {AlunoId = 2, DisciplinaId = 2 },
                    new AlunoDisciplinaModel() {AlunoId = 2, DisciplinaId = 5 },
                    new AlunoDisciplinaModel() {AlunoId = 3, DisciplinaId = 1 },
                    new AlunoDisciplinaModel() {AlunoId = 3, DisciplinaId = 2 },
                    new AlunoDisciplinaModel() {AlunoId = 3, DisciplinaId = 3 },
                    new AlunoDisciplinaModel() {AlunoId = 4, DisciplinaId = 1 },
                    new AlunoDisciplinaModel() {AlunoId = 4, DisciplinaId = 4 },
                    new AlunoDisciplinaModel() {AlunoId = 4, DisciplinaId = 5 },
                    new AlunoDisciplinaModel() {AlunoId = 5, DisciplinaId = 4 },
                    new AlunoDisciplinaModel() {AlunoId = 5, DisciplinaId = 5 },
                    new AlunoDisciplinaModel() {AlunoId = 6, DisciplinaId = 1 },
                    new AlunoDisciplinaModel() {AlunoId = 6, DisciplinaId = 2 },
                    new AlunoDisciplinaModel() {AlunoId = 6, DisciplinaId = 3 },
                    new AlunoDisciplinaModel() {AlunoId = 6, DisciplinaId = 4 },
                    new AlunoDisciplinaModel() {AlunoId = 7, DisciplinaId = 1 },
                    new AlunoDisciplinaModel() {AlunoId = 7, DisciplinaId = 2 },
                    new AlunoDisciplinaModel() {AlunoId = 7, DisciplinaId = 3 },
                    new AlunoDisciplinaModel() {AlunoId = 7, DisciplinaId = 4 },
                    new AlunoDisciplinaModel() {AlunoId = 7, DisciplinaId = 5 }
                });
        }
    }
}